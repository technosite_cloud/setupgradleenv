#!/bin/sh
export ORG_GRADLE_PROJECT_GIT_BRANCH=$(echo ${WERCKER_GIT_BRANCH} | sed -e 's@\(.*\)/.*@\1@')
export ORG_GRADLE_PROJECT_GIT_HASH_NO=$(echo ${WERCKER_GIT_COMMIT} | cut -c 1-7)
export ORG_GRADLE_PROJECT_GIT_MASTER_VER=$(cat git_master.ver)
export ORG_GRADLE_PROJECT_GIT_MASTER_COMMIT=$(cat git_master.num)
export ORG_GRADLE_PROJECT_GIT_DEVELOP_VER=$(cat git_develop.ver)
export ORG_GRADLE_PROJECT_GIT_DEVELOP_COMMIT=$(cat git_develop.num)
export ORG_GRADLE_PROJECT_GIT_FEATURE_VER=$(cat git_feature.ver)
export ORG_GRADLE_PROJECT_GIT_FEATURE_COMMIT=$(cat git_feature.num)
export ORG_GRADLE_PROJECT_ARCHIVA_USER=${ARCHIVA_USER}
export ORG_GRADLE_PROJECT_ARCHIVA_PWD=${ARCHIVA_PWD}
